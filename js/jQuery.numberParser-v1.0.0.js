$(function(){
    $.fn.numberParser = function(settings){
        var options = {
            maxLength: 19
        };
        var lastSelection = 0;

        $.extend(options, settings);

        $(this).bind('input', function(e){
            var val = $(this).val(), that = $(this),
                number = [], num,


            lastSelection = this.selectionStart;
            if(/\D/g.test(val)){
                val = $(event.currentTarget).val().replace(/\s|\D/g, '');

            }
            for(var i = 0; i < 4; i += 1){
                if(i == 3){
                    num = val.slice( i * 4, 19);
                    if(num.length){
                        number.push(num);
                    }
                }
                else{
                    num = val.slice( i * 4, (i + 1) * 4);
                    if(num){
                        number.push(num);
                    }
                }
            }



            $(this).val(number.join(' '));


            if(lastSelection + 1 == $(this).val().length){
                lastSelection = lastSelection + 1;
            }

            this.selectionStart = lastSelection;
            this.selectionEnd = this.selectionStart;
        })

    }
});